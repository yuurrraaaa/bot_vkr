class ShippingList(list):
    def add_to_cart(self, good_name):
        """" Добавление обекта в список """
        self.append(good_name)

    def clear_cart(self):
        """ Очистка корзины покупок """
        self.clear()
        return "Крозина пуста"

    def show_cart(self):
        """ Отображение содержимого корзины покупок """
        output = ""
        if len(self) > 0:
            for good in self:
                output += good + "\n"
        else:
            output = "Корзина пуста пока что"
        return output

    def total(self, price=None, total_price=None):
        """ Вычисление итоговой стоимости """
        total_price += price
        return total_price
