import sqlite3


class SQLighter:
    def __init__(self, database):
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def select_all_goods(self):
        """" Получаем все значения из 2-ух таблиц """
        with self.connection:
            return self.cursor.execute(' SELECT Title, pic, Description, Price FROM Pizza UNION ALL'
                                       ' SELECT Title, pic, Description, Price FROM Drinks').fetchall()

    def all_categories(self):
        """ Получаем все категории """
        with self.connection:
            return self.cursor.execute('SELECT Title FROM Categories').fetchall()

    def select_drink(self):
        """ Получаемя все напитки """
        with self.connection:
            return self.cursor.execute('SELECT Title, pic, Description, Price FROM Drinks').fetchall()

    def select_pizza(self):
        """ Получаем все пиццы """
        with self.connection:
            return self.cursor.execute('SELECT Title, pic, Description, Price FROM Pizza').fetchall()

    def add_customer_info(self, user_id, first_name, last_name=None, username=None):
        """ Добавляем данные о пользователе """
        db_ids = list(self.cursor.execute('SELECT id FROM Customers_info').fetchall())
        if user_id in [dbi[0] for dbi in db_ids]:
            with self.connection:
                self.cursor.execute('UPDATE Customers_Info SET "First Name" = ?, "Last Name" = ?, Username = ? '
                                    'WHERE id = ?', (first_name, last_name, username, user_id))
        else:
            with self.connection:
                self.cursor.execute('INSERT INTO Customers_info (id, "First Name", "Last Name", Username)'
                                    ' VALUES (?,?,?,?)', (user_id, first_name, last_name, username))

    def add_phone(self, user_id, mobile_num):
        """ Добавляем номер пользователя """
        with self.connection:
            self.cursor.execute('UPDATE Customers_Info SET "Mobile num." = ? WHERE id = ?', (mobile_num, user_id))

    def add_location(self, user_id, latitude, longitude):
        """ Добавляем геопозицию пользователя """
        with self.connection:
            self.cursor.execute('UPDATE Customers_Info SET Latitude = ?, Longitude = ? WHERE id = ?', (latitude,
                                                                                                       longitude,
                                                                                                       user_id))

    def add_order(self, order_list, user_id):
        """ Добавляем заказ в очередь """
        db_ids = list(self.cursor.execute('SELECT Customer_id FROM Orders').fetchall())
        if user_id in [dbi[0] for dbi in db_ids]:
            with self.connection:
                self.cursor.execute('UPDATE Orders SET Order_List = ? WHERE Customer_id = ?', (order_list, user_id))
        else:
            with self.connection:
                self.cursor.execute('INSERT INTO Orders (Order_List, Customer_id) VALUES (?,?)', (order_list, user_id))

    def close(self):
        """ Закрываем текущее соединение с БД """
        self.connection.close()
