# -*- coding: utf-8 -*-

import config
import telebot
from telebot import types
from SQLighter import SQLighter
from ShippingList import ShippingList

bot = telebot.TeleBot(config.token)
shipping_list = ShippingList()
total_price = 0


@bot.message_handler(commands=['start'])
def greetings(message):
    db = SQLighter(config.database_name)
    db.add_customer_info(message.from_user.id, message.from_user.first_name, message.from_user.last_name,
                         message.from_user.username)
    welcome_message = "Здравствуйте, " + message.chat.first_name
    bot.send_message(message.chat.id, welcome_message)

    bot.send_message(message.chat.id, help_command(message))


@bot.message_handler(commands=['help'])
def help_command(message):
    additional_message = "/help - список команд\n/menu - меню ресторана\n/about - о компании"
    bot.send_message(message.chat.id, additional_message)


@bot.message_handler(commands=['about'])
def command_about(message):
    about_message = "Данный бот предназначен для быстрого формирования заказа в пиццерии"
    bot.send_message(message.chat.id, about_message)


@bot.message_handler(commands=["menu"])
def get_menu(message):
    db = SQLighter(config.database_name)

    categories = db.all_categories()
    output = "\n".join([c[0] for c in categories])
    list_categories = [list(category) for category in categories]

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)

    for command in list_categories:
        markup.add(command[0])

    bot.send_message(message.chat.id, output, reply_markup=markup)
    db.close()


@bot.message_handler(content_types=["text"])
def text_to_command(message):
    db = SQLighter(config.database_name)
    global total_price
    global shipping_list

    db_output_list = []

    kb = types.ReplyKeyboardMarkup(resize_keyboard=True)
    list_button = types.KeyboardButton(text="Итого")
    count_button = types.KeyboardButton(text="Кол-во")
    clear_button = types.KeyboardButton(text="Очистить корзину")
    kb.row(list_button, count_button)
    kb.add(clear_button)

    if message.text == 'Drinks':
        db_output_list = db.select_drink()
    elif message.text == 'Pizza':
        db_output_list = db.select_pizza()
    elif message.text == 'Итого':
        # TODO: add payments
        bot.send_message(message.chat.id, text="В корзине:")
        bot.send_message(message.chat.id, text=shipping_list.show_cart())
        if len(shipping_list) > 0:
            ikb = types.InlineKeyboardMarkup()
            order_button = types.InlineKeyboardButton(text='Заказать', callback_data="NewData")
            ikb.add(order_button)
            bot.send_message(message.chat.id, text="Итого: " + str(total_price) + "₽", reply_markup=ikb)
    elif message.text == 'Кол-во':
        bot.send_message(message.chat.id, text="Кол-во: " + str(len(shipping_list)) + "\n" + shipping_list.show_cart())
    elif message.text == 'Очистить корзину':
        bot.send_message(message.chat.id, text=shipping_list.clear_cart())
        total_price = 0
    else:
        bot.send_message(message.chat.id, text="Неизвестная команда")

    list_goods = [list(good) for good in db_output_list]
    for good in list_goods:
        commands_markup = types.InlineKeyboardMarkup()

        add_button = types.InlineKeyboardButton(text="Добавить (" + str(good[3]) + "₽)", callback_data="Add" + good[0])
        description_button = types.InlineKeyboardButton(text="Описание", callback_data="Description" + good[0])
        commands_markup.add(add_button, description_button)
        bot.send_photo(message.chat.id, good[1])
        bot.send_message(message.chat.id, text=good[0], reply_markup=commands_markup)

    bot.send_message(message.chat.id, text="В меню -> /menu", reply_markup=kb)
    db.close()


@bot.message_handler(content_types=["contact"])
def phone(message):
    db = SQLighter(config.database_name)
    global shipping_list
    if message.contact is not None:
        if message.from_user.id == message.contact.user_id:
            db.add_phone(message.from_user.id, str(message.contact.phone_number))
            db.add_order(', '.join(shipping_list), message.from_user.id)
            shipping_list.clear_cart()
        rm = types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id,
                         "Пожалуйста, подождите, наша служба доставки свяжется с вами через пару минут",
                         reply_markup=rm)


@bot.message_handler(content_types=["location"])
def location(message):
    db = SQLighter(config.database_name)
    global shipping_list
    if message.location is not None:
        db.add_location(message.from_user.id, str(message.location.latitude), str(message.location.longitude))
        db.add_order(", ".join(shipping_list), message.from_user.id)
        shipping_list.clear_cart()
        # TODO: calculating time
        rm = types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, "Ваш заказ доставят через 30 минут", reply_markup=rm)


@bot.callback_query_handler(func=lambda call: True)
def callback_incline(call):
    db = SQLighter(config.database_name)
    global total_price
    global shipping_list

    db_output_list = list(db.select_all_goods())

    for good in db_output_list:
        if call.message:
            if call.data == "Add" + good[0]:
                shipping_list.add_to_cart(good[0])
                total_price = shipping_list.total(good[3], total_price)
                bot.answer_callback_query(callback_query_id=call.id, text=good[0] + " добавлен")
            elif call.data == "Description" + good[0]:
                bot.answer_callback_query(callback_query_id=call.id, text="Описание " + good[0])
                bot.send_message(call.message.chat.id, text=good[2])
    if call.data == "NewData":
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        button_phone = types.KeyboardButton(text="Отправить номер телефона", request_contact=True)
        button_geo = types.KeyboardButton(text="Отправить геопозицию", request_location=True)
        keyboard.add(button_phone, button_geo)
        bot.send_message(call.message.chat.id,
                         "Отправьте нам свой номер телефона или геопозицию для уточнения способа доставки!",
                         reply_markup=keyboard)


if __name__ == '__main__':
    bot.polling(none_stop=True)
